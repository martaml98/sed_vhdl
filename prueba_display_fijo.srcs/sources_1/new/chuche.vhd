library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity chuche is
  Port ( 
    clk,reset: in std_logic;
    sw0,sw1,sw2,sw3: in std_logic;
    led: out std_logic_vector(3 downto 0);
    code_unid, code_dec, code_euro: out std_logic_vector (3 downto 0);
    segment_unid, segment_dec, segment_euro: out std_logic_vector(6 downto 0)
  );
end chuche;

architecture Behavioral of chuche is

	
 COMPONENT decoder 
Port ( 
    code: in std_logic_vector(3 downto 0);
    led: out STD_LOGIC_VECTOR (6 DOWNTO 0)
);
end component;
type chuche_eleccion is (nada,pat,chup,reg, gus);
type estados is (estado_on,estado_eleccion,estado_monedas,estado_bien,estado_mal);

signal eleccion: chuche_eleccion;
signal estado,next_estado: estados;
signal led_s: std_logic_vector(3 downto 0);

signal  code_unid_s, code_dec_s, code_euro_s: std_logic_vector (3 downto 0);
signal segment_unid_s, segment_dec_s, segment_euro_s: std_logic_vector (6 downto 0);

begin

encendido: process(reset,clk)
begin 

 IF rising_edge(clk) THEN
 IF (reset = '1') THEN
 estado <= estado_on;
 ELSE
 estado <= next_estado;
 END IF;
 END IF;

end process;

NEXT_STATE_DECODE: process(estado,sw0,sw1,sw2,sw3)
begin
next_estado<=estado_on;
case(estado)is
when estado_on => 
if ( sw0='0' and sw1='0' and sw2='0'and sw3='0') then
next_estado<= estado_on;
else
next_estado<= estado_eleccion;
end if;


when estado_eleccion =>
if ( sw0='1' or sw1='1' or sw2='1'or sw3='1') then 
next_estado<= estado_eleccion;
end if;
--if (cent10='1' or cent20='1' or cent50='1' or euro1='1') then
--next_estado<= estado_monedas;
--end if;
 when others => next_estado<= estado_on;
 



end case;

end process;



OUTPUT_DECODER: process(estado,eleccion,sw0,sw1,sw2,sw3)
begin
led_s(0) <= '0';
case(estado)is 
when estado_on => led_s(0) <= '0'; 
                  led_s(1)<='0';
                  led_s(2)<='0';
                  led_s(3)<='0';

when estado_eleccion =>
if ( sw0='0' and sw1='0' and sw2='0'and sw3='0') then 
eleccion<=nada;
end if;
if sw0='1' then 
eleccion<= pat;
end if;
if sw1='1' then
eleccion<= chup;
end if;
if sw2='1' then
eleccion<= reg;
end if;
if sw3='1' then
eleccion<= gus;
end if;
case(eleccion) is
when pat => led_s(0) <= '1'; 
            led_s(1)<='0';
            led_s(2)<='0';
            led_s(3)<='0';
            code_unid_s<="0000";
            code_dec_s<="0010";
            code_euro_s<="0001";


when chup =>led_s(0)<='0';
            led_s(1)<='1';
            led_s(2)<='0';
            led_s(3)<='0';
            code_unid_s<="0000";
            code_dec_s<="0101";
            code_euro_s<="0000";
when reg => led_s(0)<='0';
            led_s(1)<='0';
            led_s(2)<='1';
            led_s(3)<='0';
            code_unid_s<="0000";
            code_dec_s<="0011";
            code_euro_s<="0000";
when gus => led_s(0)<='0';
            led_s(1)<='0';
            led_s(2)<='0';
            led_s(3)<='1';
            code_unid_s<="0000";
            code_dec_s<="0111";
            code_euro_s<="0001";


when others=> led_s(0) <= '0'; 
                  led_s(1)<='0';
                  led_s(2)<='0';
                  led_s(3)<='0';
            code_unid_s<="0000";
            code_dec_s<="0000";
            code_euro_s<="0000";

end case;

when others=> led_s(0) <= '0'; 
                  led_s(1)<='0';
                  led_s(2)<='0';
                  led_s(3)<='0';
            code_unid_s<="0000";
            code_dec_s<="0000";
            code_euro_s<="0000";
end case;
end process;

--asig: FOR i in led'range GENERATE
--led(i)<=led_s(i);
--end generate;
led(0)<=led_s(0);
led(1)<=led_s(1);
led(2)<=led_s(2);
led(3)<=led_s(3);

 code_unid<=code_unid_s;
 code_dec<=code_dec_s;
 code_euro<=code_euro_s;
 
 Inst_decoder_unid: decoder PORT MAP(
  code=>code_unid_s,
  led=>segment_unid);
Inst_decoder_dec: decoder PORT MAP(
  code=>code_dec_s,
  led=>segment_dec);
Inst_decoder_euro: decoder PORT MAP(
  code=>code_euro_s,
  led=>segment_euro);
  
end;