library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity top is
  Port ( clk: in STD_LOGIC;
        boton_reset: in STD_LOGIC;
        sw0,sw1,sw2,sw3: in std_logic;
    --cent10,cent20,cent50,euro1: in std_logic;
    --digsel: in std_logic_vector (3 downto 0);
    digctrl: out std_logic_vector (3 downto 0);
    display: out std_logic_vector (6 downto 0);
    led:out std_logic_vector(3 downto 0)
    );
end top;

architecture Behavioral of top is

--signal clk_boton: STD_LOGIC;
signal boton_sincr:STD_LOGIC;
signal boton_deb:STD_LOGIC;
--signal boton_reset: STD_LOGIC;
signal sync_in,sink_out: STD_LOGIC;

signal clk_maquina:std_logic;
signal segment_unid_s, segment_dec_s, segment_euro_s: std_logic_vector (6 downto 0);
signal  code_unid_f, code_dec_f, code_euro_f: std_logic_vector (3 downto 0);


signal display_s: std_logic_vector (6 downto 0);

--COMPONENT debouncer
--    PORT (
--    clk : in std_logic;
--    rst : in std_logic;
--    btn_in : in std_logic;
--    btn_out : out std_logic);
--END COMPONENT;

--COMPONENT sincro
--    PORT (
--sync_in: IN STD_LOGIC;
--clk: IN STD_LOGIC;
--sync_out: OUT STD_LOGIC
--);
-- END COMPONENT;

COMPONENT clk_divider
	GENERIC (frec: integer:=50000000);
	PORT(
		clk : IN std_logic;
		reset : IN std_logic;          
		clk_out : OUT std_logic
		);
	END COMPONENT;
	

	
	COMPONENT chuche
	 Port ( 
    clk,reset: in std_logic;
    sw0,sw1,sw2,sw3: in std_logic;
   -- cent10,cent20,cent50,euro1: in std_logic;
    led: out std_logic_vector(3 downto 0);
    code_unid, code_dec, code_euro: out std_logic_vector (3 downto 0);
    segment_unid, segment_dec, segment_euro: out std_logic_vector(6 downto 0)
  ); END COMPONENT;
begin

Inst_clk_divider_counter:clk_divider generic map (frec=>5000000) PORT MAP(
		clk => clk,
		reset => boton_reset,
		clk_out => clk_maquina
	);
--Inst_debouncer: debouncer PORT MAP(
--    clk=>clk_maquina,
--    rst=>boton_reset,
--    btn_in=>boton_sincr,
--    btn_out=>boton_deb
--);

--Inst_sincro: sincro PORT MAP (
--  sync_in=>boton_reset,
--  clk=>clk_maquina,
-- sync_out=>boton_sincr    
-- );
 
 Inst_maquina: chuche PORT MAP (
  clk=> clk_maquina,
  reset=> boton_deb,
    sw0=>sw0,  sw1=>sw1,  sw2=>sw2, sw3=>sw3,
    --cent10=>cent10, cent20=>cent20, cent50=>cent50 ,euro1=>euro1,
    led=>led,
     code_unid=>code_unid_f,
     code_dec=> code_dec_f,
     code_euro=>code_euro_f,
     segment_unid=>segment_unid_s,
     segment_dec=> segment_dec_s,
     segment_euro=>segment_euro_s
 );

process
begin
digctrl<="1110";
display_s<=segment_unid_s;
wait for 10ns;
digctrl<="1101";
display_s<=segment_dec_s;
wait for 10ns;
digctrl<="1011";
display_s<=segment_euro_s;

end process;

display<=display_s;
--instanciar los dos diplays
end Behavioral;



