


library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity top_tb is
end;

architecture behavioral of top_tb is

  component top
    Port ( clk: in STD_LOGIC;
          boton_reset: in STD_LOGIC;
          sw0,sw1,sw2,sw3: in std_logic;
      digctrl: out std_logic_vector (3 downto 0);
      display: out std_logic_vector (6 downto 0);
      led:out std_logic_vector(3 downto 0)
      );
  end component;

  signal clk_tb: STD_LOGIC:='0';
  signal boton_reset_tb: STD_LOGIC:='0';
  signal sw0_tb,sw1_tb,sw2_tb,sw3_tb: std_logic;
  signal digctrl_tb: std_logic_vector (3 downto 0);
  signal display_tb: std_logic_vector (6 downto 0);
  signal led_tb: std_logic_vector(3 downto 0) ;

begin

  uut: top port map ( clk         => clk_tb,
                      boton_reset => boton_reset_tb,
                      sw0         => sw0_tb,
                      sw1         => sw1_tb,
                      sw2         => sw2_tb,
                      sw3         => sw3_tb,
                      digctrl     => digctrl_tb,
                      display     => display_tb,
                      led         => led_tb );
clk_tb<=not clk_tb after 100ns;
  stimulus: process
  begin
  
    sw0_tb<='1';
  wait for 200ns;
    
  sw0_tb<='0';
  sw1_tb<='1';
  wait for 200ns;
   sw1_tb<='0';
  sw2_tb<='1';
  wait for 200ns;
  boton_reset_tb<='1';
  
    wait;
  end process;



end Behavioral;