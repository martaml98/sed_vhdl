library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity chuche_tb is
end;

architecture bench of chuche_tb is

  component chuche
    Port ( 
      clk,reset: in std_logic;
      sw0,sw1,sw2,sw3: in std_logic;
      led: out std_logic_vector(3 downto 0);
      code_unid, code_dec, code_euro: out std_logic_vector (3 downto 0);
      segment_unid, segment_dec, segment_euro: out std_logic_vector(6 downto 0)
    );
  end component;

  signal clk_tb: std_logic:='0';
  signal reset_tb: std_logic:='0';
  signal sw0_tb,sw1_tb,sw2_tb,sw3_tb: std_logic;
  signal led_tb: std_logic_vector(3 downto 0);
  signal code_unid_tb, code_dec_tb, code_euro_tb: std_logic_vector (3 downto 0);
  signal segment_unid_tb, segment_dec_tb, segment_euro_tb: std_logic_vector(6 downto 0) ;

  constant clock_period: time := 10 ns;

begin

  uut: chuche port map ( clk          => clk_tb,
                         reset        => reset_tb,
                         sw0          => sw0_tb,
                         sw1          => sw1_tb,
                         sw2          => sw2_tb,
                         sw3          => sw3_tb,
                         led          => led_tb,
                         code_unid    => code_unid_tb,
                         code_dec     => code_dec_tb,
                         code_euro    => code_euro_tb,
                         segment_unid => segment_unid_tb,
                         segment_dec  => segment_dec_tb,
                         segment_euro => segment_euro_tb );

 clk_tb<=not clk_tb after 100ns;
 
  stimulus: process
  begin
  
sw1_tb<='0';
  wait for 200ns;
  sw1_tb<='1';
  wait for 200ns;
  sw1_tb<='0';
  sw2_tb<='1';
  wait for 200ns;
  sw3_tb<='1';
  sw2_tb<='0';
  wait for 200ns;
  sw3_tb<='0';
  sw0_tb<='1';
  wait for 200ns;
  reset_tb<='1';
  wait for 200ns;
   reset_tb<='0';
   sw0_tb<='1';

    wait;
  end process;


end;
  
